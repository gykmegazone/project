package jpashop.api;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import jpashop.domain.item.Item;
import jpashop.service.ItemService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/item")
@RequiredArgsConstructor
public class ItemApiController {
	private final ItemService itemService;
	
	@ApiOperation(value = "상품 목록 조회", notes = "모든 상품 조회")
	@GetMapping("/v2/items")
	public List<Item> itemsv1() {
        return itemService.findItems();
	}
}
